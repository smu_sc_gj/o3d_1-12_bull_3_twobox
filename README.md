# o3d_1-12_bull_3_twoBox

Extended from the ogre/bullet base project for CMU students studying Game Engine Development.
Combines Ogre3d (1.12) and Bullet (3.0).

This project has more than one box and these collide, note the second box is very high up and takes ages to fall into the scene. 
